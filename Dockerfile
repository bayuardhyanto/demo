FROM golang:1.22
WORKDIR /
COPY main.go ./

RUN go build -o /main ./main.go

FROM scratch
COPY --from=0 /main /main
CMD ["/main"]
